<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name', 32);
	        $table->string('last_name', 32);
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('country', 32);
	        $table->string('gender', 16);
	        $table->date('birth_date');
	        $table->string('phone_number', 32);
	        $table->string('skype_id', 32);
	        $table->string('cv_upload');
	        $table->integer('user_role_id')->unsigned()->default(3);
	        $table->integer('user_facebook_id')->unsigned()->nullable();
			$table->integer('user_twitter_id')->unsigned()->nullable();
	        $table->date('last_login');
	        $table->integer('is_validated');
	        $table->string('validation_token');
			$table->rememberToken();
			$table->timestamps();
			
			$table->foreign('user_role_id')
			->references('id')->on('user_roles')
			->onDelete('cascade');
	        $table->foreign('user_facebook_id')
			->references('id')->on('user_facebooks')
			->onDelete('cascade');
			$table->foreign('user_twitter_id')
			->references('id')->on('user_twitters')
			->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
