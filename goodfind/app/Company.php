<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Company extends Model {

	protected $table = 'companies';

	protected $fillable = [
		'name',
		'logo',
		'category',
		'description'
	];

	public static $rules = [
		'name' => 'required',
		'logo' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
		'description' => 'required'
	];

	public static $messages = [
    	'required' => 'The :attribute field is required.',
	];

	public function isValid(){
		$validator = Validator::make($this->attributes, static::$rules, static::$messages);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

}
