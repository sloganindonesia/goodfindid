<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Article extends Model {

	protected $table = 'articles';

	protected $fillable = [
		'title',
		'slug',
		'logo',
		'body'
	];

	public static $rules = [
		'title' => 'required|min:3',
		'logo' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
		'body' => 'required'
	];

	public static $messages = [
    	'title.required' => 'You must input title!',
    	'body.required' => 'You must input body!'
	];

	public function isValid(){
		$validator = Validator::make($this->attributes, static::$rules, static::$messages);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}
}
