<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserTwitter extends Model {

	protected $table = 'user_twitters';

	protected $fillable = [
		'twitter_id',
		'token',
		'token_secret'
	];

	public static $rules = [
		'twitter_id' => 'required',
		'token' => 'required',
		'token_secret' => 'required'
	];

	public static $messages = [
    	'twitter_id.required' => 'You must input twitter_id!'
	];

	public function isValid(){
		// $validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function user() {
		return $this->belongsTo('Goodfind\User');
	}

}
