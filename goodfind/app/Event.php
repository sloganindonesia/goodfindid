<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Event extends Model {

	protected $table = 'events';

	protected $fillable = [
		'name',
		'logo',
		'category',
		'date',
		'venue',
		'description',
		'quota'
	];

	public static $rules = [
		'name' => 'required',
		'date' => 'required|date',
		'logo' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg',
		'venue' => 'required',
		'quota'	=> 'integer'
	];

	public static $messages = [
    	'name.required' => 'You must input name!',
    	'date.required' => 'You must input date!',
    	'quota.required' => 'You must input quota!'
	];

	public function isValid(){
		$validator = Validator::make($this->attributes, static::$rules, static::$messages);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function users() {
		return $this->belongsToMany('Goodfind\User', 'user_events');
	}

}
