<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserRole extends Model {

	protected $table = 'user_roles';

	protected $fillable = [
		'name'
	];

	public static $rules = [
		'name' => 'required'
	];

	public static $messages = [
    	'name.required' => 'You must input name!'
	];

	public function isValid(){
		// $validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function users() {
		return $this->hasMany('Goodfind\User');
	}

}
