<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserUniversity extends Model {

	protected $table = 'user_universities';

	protected $fillable = [
		'user_id',
		'university',
		'department',
		'major',
		'grade',
		'graduation_year'
	];

	public static $rules = [
		'university' => 'required',
		'department' => 'required',
		'major' => 'required',
		'grade' => 'required',
		'graduation_year' => 'required'
	];

	public static $messages = [
    	'name.required' => 'You must input name!',
    	'date.required' => 'You must input date!',
    	'quota.required' => 'You must input quota!'
	];

	public function isValid(){
		// $validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function users() {
		return $this->belongsTo('Goodfind\User');
	}

}
