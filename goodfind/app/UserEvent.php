<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserEvent extends Model {

	protected $table = 'user_events';

	protected $fillable = [
		'user_id',
		'event_id'
	];

	public static $rules = [
		'user_id' => 'required',
		'event_id' => 'required'
	];

	public static $messages = [
    	'user_id.required' => 'You must input user ID!'
	];

	public function isValid(){
		// $validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

}
