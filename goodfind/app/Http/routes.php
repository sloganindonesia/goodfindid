<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::model('users','User');
Route::model('vacancies','Vacancy');
Route::model('events','Event');
Route::model('articles','Article');
Route::model('companies','Company');
Route::model('userUniversities', 'UserUniversity');
Route::model('userRoles', 'UserRole');
Route::model('userFacebooks', 'UserFacebook');
Route::model('userTwitters', 'UserTwitter');
Route::model('userEvents', 'UserEvent');
Route::model('userVacancies', 'UserVacancy');

Route::get('/', 'PagesController@home');
Route::get('home', 'PagesController@home');
Route::get('app', 'PagesController@app');

Route::get('about', 'PagesController@about');
Route::get('contact-us', 'PagesController@contact');
Route::get('test', 'PagesController@test');


Route::post('loginfb', 'FBController@submit');

// Route::get('admin', ['middleware' => 'admin', function()
// {
//     return 'Hello World';
// }]);

Route::group(['middleware' => 'admin'], function(){
	Route::get('testadmin',function(){
		return 'Hello World';
	});
	Route::resource('users','UsersController');
	Route::resource('vacancies','VacanciesController');	
	Route::resource('events', 'EventsController');
	Route::resource('articles', 'ArticlesController');
	Route::resource('companies', 'CompaniesController');
	Route::resource('userUniversities', 'UserUniversitiesController');
	Route::resource('userRoles', 'UserRolesController');
	Route::resource('userEvents', 'UserEventsController');
	Route::resource('userVacancies', 'UserVacanciesController');
	Route::resource('userFacebooks', 'UserFacebooksController');
	Route::resource('userTwitters', 'UserTwittersController');
});

Route::group(['middleware' => 'staff'], function(){
	Route::get('users/create/{id}','UsersController@create');
	// Route::resource('vacancies','VacanciesController');	
	// Route::resource('events', 'EventsController');
	// Route::resource('articles', 'ArticlesController');
	// Route::resource('companies', 'CompaniesController');
});

Route::group(['middleware' => 'member'], function(){
	// Route::resource('users','UsersController');
	// Route::resource('vacancies','VacanciesController');
	// Route::resource('events', 'EventsController');
	// Route::resource('articles', 'ArticlesController');
	// Route::resource('companies', 'CompaniesController');
});

Route::resource('articles', 'ArticlesController');
Route::resource('profiles', 'ProfilesController');

Route::bind('articles', function($value, $route) {
	return Goodfind\Article::whereSlug($value)->first();
});

Route::bind('events', function($value, $route) {
	return Goodfind\Event::whereId($value)->first();
});

Route::bind('companies', function($value, $route) {
	return Goodfind\Company::whereId($value)->first();
});

Route::bind('vacancies', function($value, $route) {
	return Goodfind\Vacancy::whereSlug($value)->first();
});

Route::bind('userUniversities', function($value, $route) {
	return Goodfind\UserUniversity::whereId($value)->first();
});

Route::bind('userRoles', function($value, $route) {
	return Goodfind\UserRole::whereId($value)->first();
});

Route::bind('userFacebooks', function($value, $route) {
	return Goodfind\UserFacebook::whereId($value)->first();
});

Route::bind('userTwitters', function($value, $route) {
	return Goodfind\UserTwitter::whereId($value)->first();
});

Route::bind('userEvents', function($value, $route) {
	return Goodfind\UserEvent::whereId($value)->first();
});

Route::bind('userVacancies', function($value, $route) {
	return Goodfind\UserVacancy::whereId($value)->first();
});

Route::bind('users', function($value, $route) {
	return Goodfind\User::whereId($value)->first();
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
