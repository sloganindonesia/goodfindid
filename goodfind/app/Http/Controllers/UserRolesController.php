<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserRole;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserRolesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userRoles = UserRole::all();
		return view('userRoles.index', compact('userRoles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userRoles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRole $userRole)
	{	
		//$this->validate($request, $userRole->rules);
		$input = Input::all();
		if($userRole->fill($input)->isValid()){
			UserRole::create($input);
			return Redirect::route('userRoles.index')->with('message', 'UserRole created');
		}
		return Redirect::back()->withInput()->withErrors($userRole->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserRole $userRole)
	{
		return view('userRoles.show', compact('userRole'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserRole $userRole)
	{
		//
		return view('userRoles.edit', compact('userRole'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserRole $userRole)
	{
		$input = array_except(Input::all(), '_method');
		if($userRole->fill($input)->isValid()){
			$userRole->update($input);
			return Redirect::route('userRoles.show', $userRole->id)->with('message', 'UserRole updated.');
		}
		return Redirect::back()->withInput()->withErrors($userRole->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserRole $userRole)
	{
		$userRole->delete();
		return Redirect::route('userRoles.index')->with('message', 'UserRole deleted.');
	}

}
