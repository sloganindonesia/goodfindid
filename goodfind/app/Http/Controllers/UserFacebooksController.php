<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserFacebook;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserFacebooksController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userFacebooks = UserFacebook::all();
		return view('userFacebooks.index', compact('userFacebooks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userFacebooks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserFacebook $userFacebook)
	{	
		//$this->validate($request, $userFacebook->rules);
		$input = Input::all();
		if($userFacebook->fill($input)->isValid()){
			UserFacebook::create($input);
			return Redirect::route('userFacebooks.index')->with('message', 'UserFacebook created');
		}
		return Redirect::back()->withInput()->withErrors($userFacebook->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserFacebook $userFacebook)
	{
		return view('userFacebooks.show', compact('userFacebook'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserFacebook $userFacebook)
	{
		//
		return view('userFacebooks.edit', compact('userFacebook'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserFacebook $userFacebook)
	{
		$input = array_except(Input::all(), '_method');
		if($userFacebook->fill($input)->isValid()){
			$userFacebook->update($input);
			return Redirect::route('userFacebooks.show', $userFacebook->id)->with('message', 'UserFacebook updated.');
		}
		return Redirect::back()->withInput()->withErrors($userFacebook->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserFacebook $userFacebook)
	{
		$userFacebook->delete();
		return Redirect::route('userFacebooks.index')->with('message', 'UserFacebook deleted.');
	}

}
