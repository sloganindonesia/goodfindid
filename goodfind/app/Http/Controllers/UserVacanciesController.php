<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserVacancy;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserVacanciesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userVacancies = UserVacancy::all();
		return view('userVacancies.index', compact('userVacancies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userVacancies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserVacancy $userVacancy)
	{	
		//$this->validate($request, $userVacancy->rules);
		$input = Input::all();
		if($userVacancy->fill($input)->isValid()){
			UserVacancy::create($input);
			return Redirect::route('userVacancies.index')->with('message', 'UserVacancy created');
		}
		return Redirect::back()->withInput()->withErrors($userVacancy->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserVacancy $userVacancy)
	{
		return view('userVacancies.show', compact('userVacancy'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserVacancy $userVacancy)
	{
		//
		return view('userVacancies.edit', compact('userVacancy'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserVacancy $userVacancy)
	{
		$input = array_except(Input::all(), '_method');
		if($userVacancy->fill($input)->isValid()){
			$userVacancy->update($input);
			return Redirect::route('userVacancies.show', $userVacancy->id)->with('message', 'UserVacancy updated.');
		}
		return Redirect::back()->withInput()->withErrors($userVacancy->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserVacancy $userVacancy)
	{
		$userVacancy->delete();
		return Redirect::route('userVacancies.index')->with('message', 'UserVacancy deleted.');
	}

}
