<?php namespace Goodfind\Http\Controllers;

use Goodfind\Article;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Intervention\Image\Facades\Image;


class ArticlesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$articles = Article::all();
		return view('articles.index', compact('articles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('articles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Article $article)
	{	
		//$this->validate($request, $article->rules);
		$input = Input::all();
		if($article->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/articles/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/articles/'.$filename;

			Article::create($input);
			return Redirect::route('articles.index')->with('message', 'Article created');
		}
		return Redirect::back()->withInput()->withErrors($article->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Article $article)
	{
		return view('articles.show', compact('article'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Article $article)
	{
		//
		return view('articles.edit', compact('article'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Article $article)
	{
		$input = array_except(Input::all(), '_method');
		if($article->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/companies/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/companies/'.$filename;
			
			$article->update($input);
			return Redirect::route('articles.show', $article->slug)->with('message', 'Article updated.');
		}
		return Redirect::back()->withInput()->withErrors($article->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Article $article)
	{
		$article->delete();
		return Redirect::route('articles.index')->with('message', 'Article deleted.');
	}

}
