<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserTwitter;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserTwittersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userTwitters = UserTwitter::all();
		return view('userTwitters.index', compact('userTwitters'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userTwitters.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserTwitter $userTwitter)
	{	
		//$this->validate($request, $userTwitter->rules);
		$input = Input::all();
		if($userTwitter->fill($input)->isValid()){
			UserTwitter::create($input);
			return Redirect::route('userTwitters.index')->with('message', 'UserTwitter created');
		}
		return Redirect::back()->withInput()->withErrors($userTwitter->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserTwitter $userTwitter)
	{
		return view('userTwitters.show', compact('userTwitter'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserTwitter $userTwitter)
	{
		//
		return view('userTwitters.edit', compact('userTwitter'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserTwitter $userTwitter)
	{
		$input = array_except(Input::all(), '_method');
		if($userTwitter->fill($input)->isValid()){
			$userTwitter->update($input);
			return Redirect::route('userTwitters.show', $userTwitter->id)->with('message', 'UserTwitter updated.');
		}
		return Redirect::back()->withInput()->withErrors($userTwitter->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserTwitter $userTwitter)
	{
		$userTwitter->delete();
		return Redirect::route('userTwitters.index')->with('message', 'UserTwitter deleted.');
	}

}
