<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserEvent;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserEventsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userEvents = UserEvent::all();
		return view('userEvents.index', compact('userEvents'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userEvents.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserEvent $userEvent)
	{	
		//$this->validate($request, $userEvent->rules);
		$input = Input::all();
		/*if(UserEvent::whereEvent_id(Input::get('event_id'))->first()->count()){
			if($userEvent->fill($input)->isValid()){
				UserEvent::create($input);
				return Redirect::route('userEvents.index')->with('message', 'UserEvent created');
			}
			return Redirect::back()->withInput()->withErrors($userEvent->errors);
		}else{
			return Redirect::back()->withInput()->with('message','Event Sudah Terdaftar!');	
		}*/
		if($userEvent->fill($input)->isValid()){
				UserEvent::create($input);
				return Redirect::route('userEvents.index')->with('message', 'UserEvent created');
			}
			return Redirect::back()->withInput()->withErrors($userEvent->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserEvent $userEvent)
	{
		return view('userEvents.show', compact('userEvent'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserEvent $userEvent)
	{
		//
		return view('userEvents.edit', compact('userEvent'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserEvent $userEvent)
	{
		$input = array_except(Input::all(), '_method');
		if($userEvent->fill($input)->isValid()){
			$userEvent->update($input);
			return Redirect::route('userEvents.show', $userEvent->id)->with('message', 'UserEvent updated.');
		}
		return Redirect::back()->withInput()->withErrors($userEvent->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserEvent $userEvent)
	{
		$userEvent->delete();
		return Redirect::route('userEvents.index')->with('message', 'UserEvent deleted.');
	}

}
