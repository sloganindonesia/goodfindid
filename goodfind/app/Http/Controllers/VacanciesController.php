<?php namespace Goodfind\Http\Controllers;

use Goodfind\Vacancy;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class VacanciesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$vacancies = Vacancy::all();
		return view('vacancies.index', compact('vacancies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('vacancies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Vacancy $vacancy)
	{	
		//$this->validate($request, $vacancy->rules);
		$input = Input::all();
		if($vacancy->fill($input)->isValid()){
			Vacancy::create($input);
			return Redirect::route('vacancies.index')->with('message', 'Vacancy created');
		}
		return Redirect::back()->withInput()->withErrors($vacancy->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Vacancy $vacancy)
	{
		return view('vacancies.show', compact('vacancy'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Vacancy $vacancy)
	{
		//
		return view('vacancies.edit', compact('vacancy'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Vacancy $vacancy)
	{
		$input = array_except(Input::all(), '_method');
		if($vacancy->fill($input)->isValid()){
			$vacancy->update($input);
			return Redirect::route('vacancies.show', $vacancy->slug)->with('message', 'Vacancy updated.');
		}
		return Redirect::back()->withInput()->withErrors($vacancy->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Vacancy $vacancy)
	{
		$vacancy->delete();
		return Redirect::route('vacancies.index')->with('message', 'Vacancy deleted.');
	}

}
