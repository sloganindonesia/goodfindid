<?php namespace Goodfind\Http\Controllers;

use Goodfind\Event;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Intervention\Image\Facades\Image;

class EventsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$events = Event::all();
		return view('events.index', compact('events'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('events.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Event $event)
	{	
		//$this->validate($request, $event->rules);
		$input = Input::all();
		if($event->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/events/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/events/'.$filename;

			Event::create($input);
			return Redirect::route('events.index')->with('message', 'Event created');
		}
		return Redirect::back()->withInput()->withErrors($event->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Event $event)
	{
		return view('events.show', compact('event'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Event $event)
	{
		//
		return view('events.edit', compact('event'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Event $event)
	{
		$input = array_except(Input::all(), '_method');
		if($event->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/events/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/events/'.$filename;

			$event->update($input);
			return Redirect::route('events.show', $event->id)->with('message', 'Event updated.');
		}
		return Redirect::back()->withInput()->withErrors($event->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Event $event)
	{
		$event->delete();
		return Redirect::route('events.index')->with('message', 'Event deleted.');
	}

}
