<?php namespace Goodfind\Http\Controllers;

class PagesController extends Controller {

	public function home()
	{
		return view('home');
	}

	public function contact()
	{
		return view('contact');
	}

	public function about()
	{
		return view('about');
	}

	public function app()
	{
		return view('app2');
	}

	public function test()
	{
		return view('test');
	}

}
