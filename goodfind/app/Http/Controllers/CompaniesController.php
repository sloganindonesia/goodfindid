<?php namespace Goodfind\Http\Controllers;

use Goodfind\Company;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Intervention\Image\Facades\Image;

class CompaniesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$companies = Company::all();
		return view('companies.index', compact('companies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('companies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Company $company)
	{	
		//$this->validate($request, $company->rules);
		$input = Input::all();
		if($company->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/companies/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/companies/'.$filename;
			Company::create($input);
			return Redirect::route('companies.index')->with('message', 'Company created');
		}
		return Redirect::back()->withInput()->withErrors($company->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Company $company)
	{
		return view('companies.show', compact('company'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Company $company)
	{
		//
		return view('companies.edit', compact('company'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Company $company)
	{
		$input = array_except(Input::all(), '_method');
		if($company->fill($input)->isValid()){
			$image = Input::file('logo');
			$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			$path = 'public/img/companies/'.$filename;
        	Image::make($image->getRealPath())->resize(468,249)->save($path);
			$input->logo = 'img/companies/'.$filename;
			
			$company->update($input);
			return Redirect::route('companies.show', $company->id)->with('message', 'Company updated.');
		}
		return Redirect::back()->withInput()->withErrors($company->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Company $company)
	{
		$company->delete();
		return Redirect::route('companies.index')->with('message', 'Company deleted.');
	}

}
