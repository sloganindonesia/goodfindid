<?php namespace Goodfind\Http\Controllers;

use Goodfind\UserUniversity;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;

class UserUniversitiesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$userUniversities = UserUniversity::all();
		return view('userUniversities.index', compact('userUniversities'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('userUniversities.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserUniversity $userUniversity)
	{	
		//$this->validate($request, $userUniversity->rules);
		$input = Input::all();
		if($userUniversity->fill($input)->isValid()){
			UserUniversity::create($input);
			return Redirect::route('userUniversities.index')->with('message', 'UserUniversity created');
		}
		return Redirect::back()->withInput()->withErrors($userUniversity->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(UserUniversity $userUniversity)
	{
		return view('userUniversities.show', compact('userUniversity'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(UserUniversity $userUniversity)
	{
		//
		return view('userUniversities.edit', compact('userUniversity'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserUniversity $userUniversity)
	{
		$input = array_except(Input::all(), '_method');
		if($userUniversity->fill($input)->isValid()){
			$userUniversity->update($input);
			return Redirect::route('userUniversities.show', $userUniversity->id)->with('message', 'UserUniversity updated.');
		}
		return Redirect::back()->withInput()->withErrors($userUniversity->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(UserUniversity $userUniversity)
	{
		$userUniversity->delete();
		return Redirect::route('userUniversities.index')->with('message', 'UserUniversity deleted.');
	}

}
