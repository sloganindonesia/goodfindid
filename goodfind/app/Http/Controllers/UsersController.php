<?php namespace Goodfind\Http\Controllers;

use Goodfind\User;
use Goodfind\Http\Requests;
use Goodfind\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Input;
use Redirect;
use Intervention\Image\Facades\Image;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all();
		return view('users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	// public function store(User $user)
	// {	
	// 	//$this->validate($request, $user->rules);
	// 	$input = Input::all();
	// 	if($user->fill($input)->isValid()){
	// 		$image = Input::file('logo');
	// 		$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
	// 		$path = 'public/img/users/'.$filename;
 //        	Image::make($image->getRealPath())->resize(468,249)->save($path);
	// 		$input->logo = 'img/users/'.$filename;

	// 		User::create($input);
	// 		return Redirect::route('users.index')->with('message', 'User created');
	// 	}
	// 	return Redirect::back()->withInput()->withErrors($user->errors);
	// }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(User $user)
	{
		return view('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(User $user)
	{
		//
		return view('users.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(User $user)
	{
		$input = array_except(Input::all(), '_method');
		if($user->fill($input)->isValid()){
			//$image = Input::file('logo');
			//$filename = date('Y-m-d-H:i:s')."-".$image->getClientOriginalName();
			//$path = 'public/img/users/'.$filename;
        	//Image::make($image->getRealPath())->resize(468,249)->save($path);
			//$input->logo = 'img/users/'.$filename;
			$pass = bcrypt(Input::get('password'));
			$input['password'] = $pass;
			$user->update($input);
			return Redirect::route('users.show', $user->id)->with('message', 'User updated.');
		}
		return Redirect::back()->withInput()->withErrors($user->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(User $user)
	{
		$user->delete();
		return Redirect::route('users.index')->with('message', 'User deleted.');
	}

}
