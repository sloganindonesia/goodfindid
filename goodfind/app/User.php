<?php namespace Goodfind;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Validator;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'password',
		'country',
		'gender',
		'birth_date',
		'phone_number',
		'skype_id',
		'cv_upload',
		'user_role_id',
		'user_facebook_id',
		'user_twitter_id',
		'last_login',
		'is_validated',
		'validation_token'
	];
	
	public static $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email',
		'password' => 'required|min:8'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function isValid(){
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function events() {
		return $this->belongsToMany('Goodfind\Event', 'user_events');
	}

	public function vacancies() {
		return $this->belongsToMany('Goodfind\Vacancy', 'user_vacancies');
	}

	public function facebook() {
		return $this->hasOne('Goodfind\UserFacebook');
	}

	public function twitter() {
		return $this->hasOne('Goodfind\UserTwitter');
	}

	public function role() {
		return $this->belongsTo('Goodfind\UserRole');
	}

	public function university() {
		return $this->hasOne('Goodfind\UserUniversity');
	}

}
