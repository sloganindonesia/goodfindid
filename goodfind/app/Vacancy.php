<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Vacancy extends Model {

	protected $table = 'vacancies';

	protected $fillable = [
		'company_id',
		'title',
		'slug',
		'subtitle',
		'position',
		'deadline',
		'description'
	];

	public static $rules = [
		'title' => 'required|min:3',
		'position' => 'required',
		'slug' => 'required',
		'deadline' => 'required|date',
		'description' => 'required'
	];

	public static $messages = [
    	'title.required' => 'You must input title!',
    	'body.required' => 'You must input body!'
	];

	public function isValid(){
		//$validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function users() {
		return $this->belongsToMany('Goodfind\User', 'user_vacancies');
	}

}
