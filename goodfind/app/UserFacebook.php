<?php namespace Goodfind;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class UserFacebook extends Model {

	protected $table = 'user_facebooks';

	protected $fillable = [
		'facebook_id',
		'token'
	];

	public static $rules = [
		'facebook_id' => 'required',
		'token' => 'required'
	];

	public static $messages = [
    	'facebook_id.required' => 'You must input facebook_id!'
	];

	public function isValid(){
		// $validator = Validator::make($this->attributes, static::$rules, static::$messages);
		$validator = Validator::make($this->attributes, static::$rules);
		if($validator->passes()) return true;
		$this->errors = $validator->messages();
		return false;
	}

	public function user() {
		return $this->belongsTo('Goodfind\User');
	}

}
