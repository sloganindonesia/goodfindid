<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Let's us bring you to your imagination</title>
	<!-- core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
	
	<!--custom css-->
	<link href="css/custom.css" rel="stylesheet">
		
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage">

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="images/logo3.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">DISCOVER</a>
							<ul class="dropdown-menu">
								<li><a href="#">C O M P A N Y</a></li>
								<li><a href="#">V A C A N C Y</a></li>
								<li><a href="#">E V E N T</a></li>
							</ul>
						</li>
                        <li><a href="#features">ABOUT</a></li>
                        <li><a href="#services">CONTACT</a></li>
                        <li><a href="#portfolio">LOGIN</a>
							<ul class="dropdown-menu">
								<li><a href="#">S E T T I N G</a></li>
							<li><a href="#">L O G   O U T</a></li>
							</ul>
						</li>
                        <li class="scroll"><a href="#about">Community</a></li>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
	
	<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4"></div>
				<div class="col-sm-3"><span>LET'S FIND</span></div>
				<div class="col-sm-1"><span><a href="#"><i class="fa fa-facebook"></i></a></span></div>
				<div class="col-sm-1"><span><a href="#"><i class="fa fa-twitter"></i></a></span></div>
				<div class="col-sm-4"></div>
            </div>
        </div>
    </footer><!--/#footer-->
	
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mousescroll.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.inview.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/main.js"></script>
	
	<!--navbar-->
	<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.core.js" type="text/javascript"></script>
	<script type="text/javascript" src="scripts/scripts.js"></script>
	
	
	<!--pikachoose-->
	<link type="text/css" href="css/pika.css" rel="stylesheet" />
	<script type="text/javascript" src="js/pika/jquery.jcarousel.min.js"></script>
	<script type="text/javascript" src="js/pika/jquery.pikachoose.min.js"></script>
	<script type="text/javascript" src="js/pika/jquery.touchwipe.min.js"></script>
	<script language="javascript">
		$(document).ready(function (){
					$("#pikame").PikaChoose({carousel:true, carouselVertical:true});
				});
	</script>

</body>
</html>