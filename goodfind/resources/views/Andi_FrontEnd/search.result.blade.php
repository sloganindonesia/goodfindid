@extends('app')

@section('content')
<section id="home" data-speed="4" data-type="background">
	<div class="container">
		<hgroup class="mb20">
			<h1>Search Result</h1>
			<h2>24 result of 'technology'</h2>
			</br>
		</hgroup>
		<hr></hr>
		<section class="col-sm-12">
			<article class="search-result row">
				<div class="col-sm-9" margin-top="10px">
					<h3><a href="#">search result 1</a></h3>
					<p>blablablablablabla</p>
				</div>
				<div class="col-sm-3">
					<div>
						<ul class="meta-search" vertical-align="middle">
							<br></br>
							<li><span>In : <b>judul 1</b></span></li>
							<li><span>created : <b>24/2/2014</b></span></li>
						</ul>
					</div>
				</div>
			</article>
			<article class="search-result row">
				<div class="col-sm-9" margin-top="10px">
					<h3><a href="#">search result 1</a></h3>
					<p>blablablablablabla</p>
				</div>
				<div class="col-sm-3">
					<div padding="300px">
						<ul class="meta-search">
							<br></br>
							<li><span>In : <b>judul 1</b></span></li>
							<li><span>created : <b>24/2/2014</b></span></li>
						</ul>
					</div>
				</div>
			</article>
			<article class="search-result row">
				<div class="col-sm-9" margin-top="10px">
					<h3><a href="#">search result 1</a></h3>
					<p>blablablablablabla</p>
				</div>
				<div class="col-sm-3">
					<div padding="300px">
						<ul class="meta-search">
							<br></br>
							<li><span>In : <b>judul 1</b></span></li>
							<li><span>created : <b>24/2/2014</b></span></li>
						</ul>
					</div>
				</div>
			</article>
		</section>
	</div>
</section>
@endsection