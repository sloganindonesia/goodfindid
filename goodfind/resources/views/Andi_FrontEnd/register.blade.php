@extends('app')

@section('content')
<div class="clearfix">
	<h1>REGISTER</h1>
	<div class="login-form">
		<form class="form-horizontal" align="middle">
			<div class="form-group">
			<div class="col-sm-10">
			  <input type="email" class="form-control form-control-inline" id="inputEmail3" placeholder="Email">
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-10">
			  <input type="email" class="form-control form-control-inline" id="inputEmail" placeholder="Email">
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-10">
			  <input type="password" class="form-control form-control-inline" id="inputPassword" placeholder="Password">
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-10">
			  <input type="password" class="form-control form-control-inline" id="inputConfirmPassword" placeholder="Confirm Password">
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="button">Sign in</button>
			</div>
		  </div>
		</form>
	</div>
</div>
@endsection