@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Companies</div>

				<div class="panel-body">
					@if (!$companies->count())
						There is no companies
					@else
						@foreach( $companies as $company )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('companies.destroy', $company->id))) !!}
	                        	<a href="{{ route('companies.show', $company->id) }}">{!! $company->name !!}</a>
	                            {!! link_to_route('companies.edit', 'Edit', array($company->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('companies.create', 'Create Company') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
