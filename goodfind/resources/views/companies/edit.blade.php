@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Company</div>

				<div class="panel-body">
					{!! Form::model($company, ['method' => 'PATCH', 'route' => ['companies.update', $company->id], 'files'=>true]) !!}
        				@include('companies/partials/_form', ['submit_text' => 'Edit Company'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
