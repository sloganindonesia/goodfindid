@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create UserVacancy</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\UserVacancy, ['route' => ['userVacancies.store']]) !!}
        				@include('userVacancies/partials/_form', ['submit_text' => 'Create UserVacancy'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
