@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserVacancy ke-{!! $userVacancy->id !!}</div>

				<div class="panel-body">
					<p>User ID : {!! $userVacancy->user_id !!}</p>
					<p>Vacancy ID : {!! $userVacancy->vacancy_id !!}</p>
    				{!! link_to_route('userVacancies.index', 'Back to UserVacancies') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
