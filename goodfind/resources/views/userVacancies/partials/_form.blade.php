<div class="form-group">
    {!! Form::label('user_id', 'User ID:') !!}
    {!! Form::text('user_id') !!}
    {!! $errors->first('user_id') !!}
</div>
<div class="form-group">
    {!! Form::label('vacancy_id', 'Vacancy ID:') !!}
    {!! Form::text('vacancy_id') !!}
    {!! $errors->first('vacancy_id') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>