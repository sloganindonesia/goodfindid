@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserVacancies</div>

				<div class="panel-body">
					@if (!$userVacancies->count())
						There is no userVacancies
					@else
						@foreach( $userVacancies as $userVacancy )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userVacancies.destroy', $userVacancy->id))) !!}
	                        	<a href="{{ route('userVacancies.show', $userVacancy->id) }}">{!! $userVacancy->user_id !!}</a>
	                            {!! link_to_route('userVacancies.edit', 'Edit', array($userVacancy->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userVacancies.create', 'Create UserVacancy') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
