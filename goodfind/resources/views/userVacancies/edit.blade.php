@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserVacancy</div>

				<div class="panel-body">
					{!! Form::model($userVacancy, ['method' => 'PATCH', 'route' => ['userVacancies.update', $userVacancy->id]]) !!}
        				@include('userVacancies/partials/_form', ['submit_text' => 'Edit UserVacancy'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
