@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserFacebooks</div>

				<div class="panel-body">
					@if (!$userFacebooks->count())
						There is no userFacebooks
					@else
						@foreach( $userFacebooks as $userFacebook )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userFacebooks.destroy', $userFacebook->id))) !!}
	                        	<a href="{{ route('userFacebooks.show', $userFacebook->id) }}">{!! $userFacebook->facebook_id !!}</a>
	                            {!! link_to_route('userFacebooks.edit', 'Edit', array($userFacebook->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userFacebooks.create', 'Create UserFacebook') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
