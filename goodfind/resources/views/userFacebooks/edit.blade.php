@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserFacebook</div>

				<div class="panel-body">
					{!! Form::model($userFacebook, ['method' => 'PATCH', 'route' => ['userFacebooks.update', $userFacebook->id]]) !!}
        				@include('userFacebooks/partials/_form', ['submit_text' => 'Edit UserFacebook'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
