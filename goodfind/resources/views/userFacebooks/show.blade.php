@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserFacebook ke-{!! $userFacebook->id !!}</div>

				<div class="panel-body">
					<p id="facebook_id">Facebook ID : {!! $userFacebook->facebook_id !!}</p>
					<p id="token">Token : {!! $userFacebook->token !!}</p>
    				{!! link_to_route('userFacebooks.index', 'Back to UserFacebooks') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
