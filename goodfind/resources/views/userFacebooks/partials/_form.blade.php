<div class="form-group">
    {!! Form::label('facebook_id', 'Facebook ID:') !!}
    {!! Form::text('facebook_id') !!}
    {!! $errors->first('facebook_id') !!}
</div>
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    {!! Form::text('token') !!}
    {!! $errors->first('token') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>