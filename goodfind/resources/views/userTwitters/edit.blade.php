@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserTwitter</div>

				<div class="panel-body">
					{!! Form::model($userTwitter, ['method' => 'PATCH', 'route' => ['userTwitters.update', $userTwitter->id]]) !!}
        				@include('userTwitters/partials/_form', ['submit_text' => 'Edit UserTwitter'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
