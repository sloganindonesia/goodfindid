<div class="form-group">
    {!! Form::label('twitter_id', 'Twitter ID:') !!}
    {!! Form::text('twitter_id') !!}
    {!! $errors->first('twitter_id') !!}
</div>
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    {!! Form::text('token') !!}
    {!! $errors->first('token') !!}
</div>
<div class="form-group">
    {!! Form::label('token_secret', 'Token Secret:') !!}
    {!! Form::text('token_secret') !!}
    {!! $errors->first('token_secret') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>