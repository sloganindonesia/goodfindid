@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserTwitters</div>

				<div class="panel-body">
					@if (!$userTwitters->count())
						There is no userTwitters
					@else
						@foreach( $userTwitters as $userTwitter )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userTwitters.destroy', $userTwitter->id))) !!}
	                        	<a href="{{ route('userTwitters.show', $userTwitter->id) }}">{!! $userTwitter->twitter_id !!}</a>
	                            {!! link_to_route('userTwitters.edit', 'Edit', array($userTwitter->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userTwitters.create', 'Create UserTwitter') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
