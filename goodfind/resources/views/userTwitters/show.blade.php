@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserTwitter ke-{!! $userTwitter->id !!}</div>

				<div class="panel-body">
					<p>Twitter ID : {!! $userTwitter->twitter_id !!}</p>
					<p>Token : {!! $userTwitter->token !!}</p>
					<p>Token Secret : {!! $userTwitter->token_secret !!}</p>
    				{!! link_to_route('userTwitters.index', 'Back to UserTwitters') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
