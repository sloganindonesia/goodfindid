<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Goodfind Asia</title>

	<!-- CSS -->
	<link href="/css/app.css" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css" type="text/css">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" href="css/custom.css" type="text/css">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<!-- 
	<meta charset="UTF-8">
	<title>About - Law Firm Web Template</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
	<title>untitled</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	 -->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	{!! HTML::script('js/jquery-2.1.1.min'); !!}
	{!! HTML::script('js/fbload.js'); !!}
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Goodfind Asia</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">DISCOVER<span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>{!! HTML::link('/vacancies', 'Vacancies') !!}</li>
							<li>{!! HTML::link('/events', 'Events') !!}</li>
							<li>{!! HTML::link('/articles', 'Articles') !!}</li>
							<li>{!! HTML::link('/companies', 'Companies') !!}</li>
						</ul>
					</li>
					<li>{!! HTML::link('/about', 'ABOUT') !!}</li>
					<li>{!! HTML::link('/contact-us', 'CONTACT') !!}</li>
					@if (Auth::guest())
						<li>{!! HTML::link('/auth/login', 'SIGN IN') !!}</li>
						<!-- <li>{!! HTML::link('/auth/register', 'REGISTER') !!}</li> -->
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">PROFILE<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li>{!! HTML::link('/profiles', 'USER PAGE') !!}</li>
								<li>{!! HTML::link('/auth/logout', 'SIGN OUT') !!}</li>
							</ul>
						</li>
					@endif
				</ul>

				<!-- <ul class="nav navbar-nav navbar-right">
					
				</ul> -->
			</div>
		</div>
	</nav>

<!-- <nav>
<ul id="nav">
	<li><a href="#"><img src="images/logogf.png" style="padding:0;margin:0;"></img></a></li>
	<li><a href="#">D I S C O V E R</a>
		<ul>
			<li><a href="#">C O M P A N Y</a></li>
			<li><a href="#">V A C A N C Y</a></li>
			<li><a href="#">E V E N T</a></li>
		</ul>
	</li>
	<li><a href="#" style="padding-left:32%;">A B O U T</a></li>
	<li><a href="#">C O N T A CT</a></li>
	<li><a href="#">L O G I N</a>
		<ul>
			<li><a href="#">S E T T I N G</a></li>
			<li><a href="#">L O G   O U T</a></li>
		</ul>
	</li>
	<li><a href="#">
		<input type="text" style="width:200px;height:30px;float:left" value="">
		<button type="button" class="btn btn-default btn-lg">
		<span class="glyphicon glyphicon-search" style="float-right" aria-hidden="true"></span></button>
	</a></li>		
</ul>
     </nav>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js" type="text/javascript" charset="utf-8"></script>	
<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.core.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/scripts.js"></script>
 -->

<!--
	<nav id="header">
		<div class="clearfix">
			<div class="logo">
				<a href="index.html"><img src="images/logo.png" alt="LOGO" height="52" width="362"></a>
			</div>
			<ul class="navigation">
				<li class="active">
					<a href="index.html">{!! HTML::link('/', 'Home') !!}</a>
				</li>
				<li>
					<a href="about.html">{!! HTML::link('/articles', 'Articles') !!}</a>
				</li>
				<li>
					<a href="practices.html">{!! HTML::link('/contact-us', 'Contact Us') !!}</a>
				</li>
				<li>
					<a href="lawyers.html">LOGIN</a>
				</li>
				<li>
					<a href="news.html">SEARCH BOX</a>
					<div>
						<a href="singlepost.html">News Single Post</a>
					</div>
				</li>
				<li>
					<a href="contact.html">Contact</a>
				</li>
			</ul>
		</div>
	</nav>
-->

	@if (Session::has('message'))
		<div class="flash alert-info">
			<p>{{ Session::get('message') }}</p>
		</div>
	@endif
	<br><br><br>
	@yield('content')

	<footer class="footer">
		<nav class="navbar navbar-default navbar-fixed-bottom">
		        <div class="navbar-inner navbar-content-center text-center">
		            <!-- <p class="text-muted credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p> -->
		            <h4>LET'S FIND</h4>
		        </div>
    	</nav>
	</footer>

	<!-- <div id="footer">
		<div class="clearfix">
			<div class="section">
				<h4>Latest News</h4>
				<p>
					This website template has been designed by Free Website Templates for you, for free. You can replace all this text with your own text. You can remove any link.
				</p>
			</div>
			<div class="section contact">
				<h4>Contact Us</h4>
				<p>
					<span>Address:</span> the address city, state 1111
				</p>
				<p>
					<span>Phone:</span> (+20) 000 222 999
				</p>
				<p>
					<span>Email:</span> info@freewebsitetemplates.com
				</p>
			</div>
			<div class="section">
				<h4>SEND US A MESSAGE</h4>
				<p>
					If you're having problems editing this website template, then don't hesitate to ask for help on the Forums.
				</p>
				<a href="http://www.freewebsitetemplates.com/misc/contact/" class="subscribe">Click to send us an email</a>
			</div>
		</div>
		<div id="footnote">
			<div class="clearfix">
				<div class="connect">
					<a href="http://freewebsitetemplates.com/go/facebook/" class="facebook"></a><a href="http://freewebsitetemplates.com/go/twitter/" class="twitter"></a><a href="http://freewebsitetemplates.com/go/googleplus/" class="googleplus"></a><a href="http://pinterest.com/fwtemplates/" class="pinterest"></a>
				</div>
				<p>
					c Copyright 2023 Manes Winchester. All Rights Reserved.
				</p>
			</div>
		</div>
	</div>
 -->	
	
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
