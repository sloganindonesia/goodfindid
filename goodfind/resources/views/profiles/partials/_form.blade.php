<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name') !!}
    {!! $errors->first('first_name') !!}
</div>
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name') !!}
    {!! $errors->first('last_name') !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email') !!}
    {!! $errors->first('email') !!}
</div>
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password') !!}
    {!! $errors->first('password') !!}
</div>
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::select('country', array('IND' => 'Indonesia', 'JPN' => 'Japan')) !!}
    {!! $errors->first('country') !!}
</div>
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::select('gender', array('M' => 'Male', 'F' => 'Female'), 'M') !!}
    {!! $errors->first('gender') !!}
</div>
<div class="form-group">
    {!! Form::label('birth_date', 'Birth Date:') !!}
    {!! Form::text('birth_date') !!}
    {!! $errors->first('birth_date') !!}
</div>
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::text('phone_number') !!}
    {!! $errors->first('phone_number') !!}
</div>
<div class="form-group">
    {!! Form::label('skype_id', 'Skype ID:') !!}
    {!! Form::text('skype_id') !!}
    {!! $errors->first('skype_id') !!}
</div>
<div class="form-group">
    {!! Form::label('cv_upload', 'CV Upload:') !!}
    {!! Form::text('cv_upload') !!}
    {!! $errors->first('cv_upload') !!}
</div>
<div class="form-group">
    {!! Form::label('user_role_id', 'User Role ID:') !!}
    {!! Form::text('user_role_id') !!}
    {!! $errors->first('user_role_id') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>