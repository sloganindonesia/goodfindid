@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit User</div>

				<div class="panel-body">
					{!! Form::model($user, ['method' => 'PATCH', 'route' => ['users.update', $user->id], 'files'=>true]) !!}
        				@include('users/partials/_form', ['submit_text' => 'Edit User'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
