@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">BIO AND CONTACT</div>

				<div class="panel-body">
					
					<p>First Name : {!! $user->first_name !!}</p>
					@if ($user->last_name!=null)
						<p>Last Name : {!! $user->last_name !!}</p>
					@else

					@endif
					<p>Birth Date : {!! $user->birth_date !!}</p>
					<p>Gender : {!! $user->gender !!}</p>
					<p>Phone : {!! $user->phone_number !!}</p>
					<p>Email : {!! $user->email !!}</p>
					<p>Skype ID : {!! $user->skype_id !!}</p>
					<p>CV Upload : {!! $user->cv_upload !!}</p>
				</div>

				<div class="panel-heading">Education Information</div>
				<div class="panel-body">
					@if ($university->count()==0)
						<p>You haven't complete your education information!</p>
						<a href="#">Edit Education Information</a>
					@else
						<p>Department : {!! $university->first()->department !!}</p>
						<p>Major : {!! $university->first()->major !!}</p>
						<p>Year in : {!! $university->first()->grade !!}</p>
						<p>Year Graduated : {!! $university->first()->graduation_year !!}</p>
						<p>University : {!! $university->first()->university !!}</p>
						<p>Country : {!! $user->country !!}</p>
					@endif
				</div>

				<div class="panel-heading">Joined Event</div>
				<div class="panel-body">
					<p>Event Name : {!! $events->first()->name !!}</p>
				</div>

				<div class="panel-heading">Applied Vacancies</div>
				<div class="panel-body">
					<p>Job : {!! $events->first()->name !!}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
