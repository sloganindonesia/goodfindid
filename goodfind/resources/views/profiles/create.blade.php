@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create User</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\User, ['route' => ['users.store'], 'files'=>true]) !!}
        				@include('users/partials/_form', ['submit_text' => 'Create User'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
9