@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserEvent</div>

				<div class="panel-body">
					{!! Form::model($userEvent, ['method' => 'PATCH', 'route' => ['userEvents.update', $userEvent->id]]) !!}
        				@include('userEvents/partials/_form', ['submit_text' => 'Edit UserEvent'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
