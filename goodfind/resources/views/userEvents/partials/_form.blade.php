<div class="form-group">
    {!! Form::label('user_id', 'User ID:') !!}
    {!! Form::text('user_id') !!}
    {!! $errors->first('user_id') !!}
</div>
<div class="form-group">
    {!! Form::label('event_id', 'Event ID:') !!}
    {!! Form::text('event_id') !!}
    {!! $errors->first('event_id') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>