@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserEvent ke-{!! $userEvent->id !!}</div>

				<div class="panel-body">
					<p>User ID : {!! $userEvent->user_id !!}</p>
					<p>Event ID : {!! $userEvent->event_id !!}</p>
    				{!! link_to_route('userEvents.index', 'Back to UserEvents') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
