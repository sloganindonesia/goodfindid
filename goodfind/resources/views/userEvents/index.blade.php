@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserEvents</div>

				<div class="panel-body">
					@if (!$userEvents->count())
						There is no userEvents
					@else
						@foreach( $userEvents as $userEvent )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userEvents.destroy', $userEvent->id))) !!}
	                        	<a href="{{ route('userEvents.show', $userEvent->id) }}">{!! $userEvent->user_id !!}</a>
	                            {!! link_to_route('userEvents.edit', 'Edit', array($userEvent->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userEvents.create', 'Create UserEvent') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
