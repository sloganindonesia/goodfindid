@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create UserEvent</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\UserEvent, ['route' => ['userEvents.store']]) !!}
        				@include('userEvents/partials/_form', ['submit_text' => 'Create UserEvent'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
