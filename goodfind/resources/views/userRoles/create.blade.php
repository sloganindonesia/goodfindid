@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create UserRole</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\UserRole, ['route' => ['userRoles.store']]) !!}
        				@include('userRoles/partials/_form', ['submit_text' => 'Create UserRole'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
