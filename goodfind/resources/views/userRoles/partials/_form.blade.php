<div class="form-group">
    {!! Form::label('name', 'Role Name:') !!}
    {!! Form::text('name') !!}
    {!! $errors->first('name') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>