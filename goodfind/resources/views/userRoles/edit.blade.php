@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserRole</div>

				<div class="panel-body">
					{!! Form::model($userRole, ['method' => 'PATCH', 'route' => ['userRoles.update', $userRole->id]]) !!}
        				@include('userRoles/partials/_form', ['submit_text' => 'Edit UserRole'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
