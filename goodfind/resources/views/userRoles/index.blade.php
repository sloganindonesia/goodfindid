@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserRoles</div>

				<div class="panel-body">
					@if (!$userRoles->count())
						There is no userRoles
					@else
						@foreach( $userRoles as $userRole )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userRoles.destroy', $userRole->id))) !!}
	                        	<a href="{{ route('userRoles.show', $userRole->id) }}">{!! $userRole->name !!}</a>
	                            {!! link_to_route('userRoles.edit', 'Edit', array($userRole->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userRoles.create', 'Create UserRole') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
