@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserRole ke-{!! $userRole->id !!}</div>

				<div class="panel-body">
					<p id="name">UserRole Name : {!! $userRole->name !!}</p>
    				{!! link_to_route('userRoles.index', 'Back to UserRoles') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
