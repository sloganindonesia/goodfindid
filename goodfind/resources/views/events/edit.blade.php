@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Event</div>

				<div class="panel-body">
					{!! Form::model($event, ['method' => 'PATCH', 'route' => ['events.update', $event->id], 'files'=>true]) !!}
        				@include('events/partials/_form', ['submit_text' => 'Edit Event'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
