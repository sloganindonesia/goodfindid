@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create Event</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\Event, ['route' => ['events.store'], 'files'=>true]) !!}
        				@include('events/partials/_form', ['submit_text' => 'Create Event'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
