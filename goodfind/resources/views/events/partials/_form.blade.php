<div class="form-group">
    {!! Form::label('name', 'Event Name:') !!}
    {!! Form::text('name') !!}
    {!! $errors->first('name') !!}
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo', ['accept'=>'image/*']) !!}
    {!! $errors->first('logo') !!}
</div>
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::text('category') !!}
    {!! $errors->first('category') !!}
</div>
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date') !!}
    {!! $errors->first('date') !!}
</div>
<div class="form-group">
    {!! Form::label('venue', 'Venue:') !!}
    {!! Form::text('venue') !!}
    {!! $errors->first('venue') !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description') !!}
    {!! $errors->first('description') !!}
</div>
<div class="form-group">
    {!! Form::label('quota', 'Quota:') !!}
    {!! Form::text('quota') !!}
    {!! $errors->first('quota') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>