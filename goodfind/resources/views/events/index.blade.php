@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Events</div>

				<div class="panel-body">
					@if (!$events->count())
						There is no events
					@else
						@foreach( $events as $event )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('events.destroy', $event->id))) !!}
	                        	<a href="{{ route('events.show', $event->id) }}">{!! $event->name !!}</a>
	                            {!! link_to_route('events.edit', 'Edit', array($event->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('events.create', 'Create Event') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
