@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Event ke-{!! $event->id !!}</div>

				<div class="panel-body">
					<p id="name">Event Name : {!! $event->name !!}</p>
					<p id="description">Logo : {!! $event->logo !!}</p>
					<p id="category">Category : {!! $event->category !!}</p>
					<p id="date">Event Date : {!! $event->date !!}</p>
					<p id="venue">Venue : {!! $event->venue !!}</p>
					<p id="description">Description : {!! $event->description !!}</p>
					<p id="quota">Quota : {!! $event->quota !!}</p>
    				{!! link_to_route('events.index', 'Back to Events') !!}
					<div class="fb-share-button" data-href="{{ Request::url() }}" data-layout="button_count"></div>
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="{{ Request::url() }}" data-text="Check out new event from Goodfind Indonesia" data-via="GoodfindID">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
