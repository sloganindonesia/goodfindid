@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">User ke-{!! $user->id !!}</div>

				<div class="panel-body">
					<p>First Name : {!! $user->first_name !!}</p>
					<p>Last Name : {!! $user->last_name !!}</p>
					<p>Email : {!! $user->email !!}</p>
					<p>Password : {!! $user->password !!}</p>
					<p>Country : {!! $user->country !!}</p>
					<p>Birth Date : {!! $user->birth_date !!}</p>
					<p>Phone Number : {!! $user->phone_number !!}</p>
					<p>Skype ID : {!! $user->skype_id !!}</p>
					<p>CV Upload : {!! $user->cv_upload !!}</p>
					<p>User Role ID : {!! $user->user_role_id !!}</p>
					<p>User Facebook ID : {!! $user->user_facebook_id !!}</p>
					<p>User Twitter ID : {!! $user->user_twitter_id !!}</p>
    				{!! link_to_route('users.index', 'Back to Users') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
