@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Users</div>

				<div class="panel-body">
					@if (!$users->count())
						There is no users
					@else
						@foreach( $users as $user )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('users.destroy', $user->id))) !!}
	                        	<a href="{{ route('users.show', $user->id) }}">{!! $user->email !!}</a>
	                            {!! link_to_route('users.edit', 'Edit', array($user->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('users.create', 'Create User') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
