@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Vacancies</div>

				<div class="panel-body">
					@if (!$vacancies->count())
						There is no vacancies
					@else
						@foreach( $vacancies as $vacancy )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('vacancies.destroy', $vacancy->slug))) !!}
	                        	<a href="{{ route('vacancies.show', $vacancy->slug) }}">{{ $vacancy->title }}</a>
	                            {!! link_to_route('vacancies.edit', 'Edit', array($vacancy->slug), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('vacancies.create', 'Create Vacancy') !!}
				</div>

			</div>
		</div>
	</div>
</div>
@endsection
