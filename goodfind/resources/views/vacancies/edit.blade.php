@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Vacancy</div>

				<div class="panel-body">
					{!! Form::model($vacancy, ['method' => 'PATCH', 'route' => ['vacancies.update', $vacancy->slug]]) !!}
        				@include('vacancies/partials/_form', ['submit_text' => 'Edit Vacancy'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
