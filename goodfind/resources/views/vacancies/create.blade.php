@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Create Vacancy</div>

				<div class="panel-body">
					{!! Form::model(new Goodfind\Vacancy, ['route' => ['vacancies.store']]) !!}
        				@include('vacancies/partials/_form', ['submit_text' => 'Create Vacancy'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
