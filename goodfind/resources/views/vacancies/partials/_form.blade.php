<div class="form-group">
    {!! Form::label('company_id', 'Company ID:') !!}
    {!! Form::text('company_id') !!}
    {!! $errors->first('company_id') !!}
</div>
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title') !!}
    {!! $errors->first('title') !!}
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug') !!}
    {!! $errors->first('slug') !!}
</div>
<div class="form-group">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    {!! Form::text('subtitle') !!}
    {!! $errors->first('subtitle') !!}
</div>
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::text('position') !!}
    {!! $errors->first('position') !!}
</div>
<div class="form-group">
    {!! Form::label('deadline', 'Deadline:') !!}
    {!! Form::text('deadline') !!}
    {!! $errors->first('deadline') !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description') !!}
    {!! $errors->first('description') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>