<div class="form-group">
    {!! Form::label('user_id', 'User ID:') !!}
    {!! Form::text('user_id') !!}
    {!! $errors->first('user_id') !!}
</div>
<div class="form-group">
    {!! Form::label('university', 'University Name:') !!}
    {!! Form::text('university') !!}
    {!! $errors->first('university') !!}
</div>
<div class="form-group">
    {!! Form::label('department', 'Department:') !!}
    {!! Form::text('department') !!}
    {!! $errors->first('department') !!}
</div>
<div class="form-group">
    {!! Form::label('major', 'Major:') !!}
    {!! Form::text('major') !!}
    {!! $errors->first('major') !!}
</div>
<div class="form-group">
    {!! Form::label('grade', 'Grade:') !!}
    {!! Form::text('grade') !!}
    {!! $errors->first('grade') !!}
</div>
<div class="form-group">
    {!! Form::label('graduation_year', 'Graduation Year:') !!}
    {!! Form::text('graduation_year') !!}
    {!! $errors->first('graduation_year') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>