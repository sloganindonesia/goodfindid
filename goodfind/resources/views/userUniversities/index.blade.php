@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserUniversities</div>

				<div class="panel-body">
					@if (!$userUniversities->count())
						There is no userUniversities
					@else
						@foreach( $userUniversities as $userUniversity )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('userUniversities.destroy', $userUniversity->id))) !!}
	                        	<a href="{{ route('userUniversities.show', $userUniversity->id) }}">{!! $userUniversity->university !!}</a>
	                            {!! link_to_route('userUniversities.edit', 'Edit', array($userUniversity->id), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('userUniversities.create', 'Create UserUniversity') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
