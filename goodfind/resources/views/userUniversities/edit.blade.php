@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit UserUniversity</div>

				<div class="panel-body">
					{!! Form::model($userUniversity, ['method' => 'PATCH', 'route' => ['userUniversities.update', $userUniversity->id]]) !!}
        				@include('userUniversities/partials/_form', ['submit_text' => 'Edit UserUniversity'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
