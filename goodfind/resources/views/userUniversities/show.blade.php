@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">UserUniversity ke-{!! $userUniversity->id !!}</div>

				<div class="panel-body">
					<p id="name">UserUniversity User ID : {!! $userUniversity->user_id !!}</p>
					<p id="description">University : {!! $userUniversity->university !!}</p>
					<p id="category">Department : {!! $userUniversity->department !!}</p>
					<p id="date">Major : {!! $userUniversity->major !!}</p>
					<p id="venue">Grade : {!! $userUniversity->grade !!}</p>
					<p id="description">Graduation Year : {!! $userUniversity->graduation_year !!}</p>
    				{!! link_to_route('userUniversities.index', 'Back to UserUniversities') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
