<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title') !!}
    {!! $errors->first('title') !!}
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug') !!}
    {!! $errors->first('slug') !!}
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo', ['accept'=>'image/*']) !!}
    {!! $errors->first('logo') !!}
</div>
<div class="form-group">
    {!! Form::label('body', 'Body:') !!}
    {!! Form::text('body') !!}
    {!! $errors->first('body') !!}
</div>
<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>