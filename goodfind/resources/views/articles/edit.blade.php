@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Edit Article</div>

				<div class="panel-body">
					{!! Form::model($article, ['method' => 'PATCH', 'route' => ['articles.update', $article->slug], 'files'=>true]) !!}
        				@include('articles/partials/_form', ['submit_text' => 'Edit Article'])
    				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
