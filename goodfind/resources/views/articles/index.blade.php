@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Articles</div>

				<div class="panel-body">
					@if (!$articles->count())
						There is no articles
					@else
						@foreach( $articles as $article )	
              			<li>
                    		{!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('articles.destroy', $article->slug))) !!}
	                        	<a href="{{ route('articles.show', $article->slug) }}">{{ $article->title }}</a>
	                            {!! link_to_route('articles.edit', 'Edit', array($article->slug), array('class' => 'btn btn-info')) !!} 
	                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    		{!! Form::close() !!}
                		</li>
            			@endforeach
					@endif
					{!! link_to_route('articles.create', 'Create Article') !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
